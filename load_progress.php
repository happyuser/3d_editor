<?php

/**
 * @name load progress, time count down
 * @author 
 * @copyright 2016
 */

if(array_key_exists('seconds',$_GET))
	$seconds = $_GET['seconds'];

?>
<?php /*
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="" />

	<title> load progress, time count down</title>
	

</head>

<body> */
?>
	<!--script src="./include/jquery-1.6.2.min.js"></script-->

         
        <style>
		
		.progress {
		  width: 100%;
		  height: 50px;/*50px;*/
		}
		.progress-wrap {
		  background: #ddd;
		  margin: 20px 0;
		  
		  overflow: hidden;
		  position: relative;
		  bottom:160px;
		}
		.progress-bar {
		    background: #f80;
		    left: 0;
		    position: absolute;
		    top: 0;
		  }
		
		#count_down {
			text-align: center;
			vertical-align: middle;
    		padding: 10px;
			left: 20%;
			top: 10px;
			z-index: 10;
			font-size: 50px;
			font-family: tahoma;
			font-weight: bold;
		}
		
		#count_down_wpapper{
			position: relative;
			
			z-index: 10;
		}

		</style>
        
        
        
        <!-- Change the below data attribute to play -->
<div class="progress-wrap progress">
<div id="count_down_wpapper">
<div id="count_down"> approximately <label id="minutes">00</label>:<label id="seconds">00</label> left to view result </div>
</div>
  <div class="progress-bar progress"></div>
</div>
        
        
    <script type="text/javascript">
    function print_warning(w){
    	$('#count_down').html('Warning! '+w);
    }
    
    function print_error (e){
    	$('#count_down').html('Error! '+e);
    }
    
    
    function start_count_down(sec_0, end_time_estimate)
    {
    	//alert(end_time_estimate);
    	$('.progress-bar').width('0%');
    	$('.progress-bar').height($('.progress-wrap').height());
    	 count_down_show();
		$('#count_down').html('approximately <label id="minutes">00</label>:<label id="seconds">00</label> left to view result');
    	$("#seconds").html(pad(sec_0%60));
		$("#minutes").html(pad(parseInt(sec_0/60,10)));
		
		    
		if(end_time_estimate)
		{
			function time_to_str(myDate)
			{
				function add_0(m)
				{
					return m<10?'0'+m:m;
				}
				
				var m = add_0(myDate.getMinutes());
				var s = add_0(myDate.getSeconds());
				return (myDate.getHours() + ":" + m + ":" + s);
			}
		
			
			var t = new Date();
			t.setSeconds(t.getSeconds() + sec_0);
			finish_time_estimate = time_to_str(t);
			$('#count_down').html('Working till about'+finish_time_estimate);
			setTimeout(function(){;},1000);
			//alert('Come back at about '+finish_time_estimate);
			console.log('Come back at about '+finish_time_estimate);
			return;
			
		}
		else 
		{
			//
		}
    	
        var sec = sec_0;
        console.log('timer started for ' + sec + ' seconds');
		function pad ( val ) { return val > 9 ? val : "0" + val; }
		
		var interval_id = setInterval( function(){
			sec = sec - 1;
			if(sec < 0) 
			{
				//$('.progress-wrap').hide();
				$('.progress-bar').hide();
				$('#count_down').html('result should be viewable in a while, if not - <a href="?report">report us</a>');
				clearInterval(interval_id);
				
			}
		    $("#seconds").html(pad(sec%60));
		    $("#minutes").html(pad(parseInt(sec/60,10)));
		    
            var getPercent = Math.round((sec_0 - sec)/sec_0 * 100);
        var getProgressWrapWidth = $('.progress-wrap').width();
        var progressTotal = getPercent ;
        $('.progress-bar').width(progressTotal+'%'); 

		}, 1000);
	}
	
	function count_down_hide()
	{
		$('.progress-wrap').hide();
	}
	
	function count_down_show()
	{
		$('.progress-wrap').show();
	}
	
	// on page load..
    //start_count_down(10);
    

    </script>
<?php /*
</body>
</html>
*/ ?>
	
       