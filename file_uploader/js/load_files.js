// When the server is ready...
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

    jQuery(document).ready(function ($) {
        'use strict';
        
        // Define the url to send the image data to
        var url = 'files.php';
        
        // Call the fileupload widget and set some parameters
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                // Add each uploaded file name to the #files list
                $.each(data.result.files, function (index, file) {
                    $.ajax({
    url:'files/'+file.name,
    type:'HEAD',
    error: function()
    {
        var error_message = 'file is not stl or obj file type or it is to big for the server options';
                        $('<li/>').html(error_message).appendTo('#files');
        //file not exists
    },
    success: function()
    {
        //file exists
        //var href = 'canvas_page.php?load='+file.name+'&x_lattices_count=2';
                	var link = '<a class="add_user_3d" name="'+file.name+'" onclick="load_user_file(event,$(this))"> fill '+file.name+' with latices</a>';
                    $('<li/>').html(link).appendTo('#files');
                    
                   // $('.add_user_3d').on('click',function(e){
//						load_user_file(e,$(this));
//					});

    }
});
                    
//                    if(UrlExists('files/'+file.name))
                });
            },
            progressall: function (e, data) {
                // Update the progress bar while files are being uploaded
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css(
                    'width',
                    progress + '%'
                );
            }
        });
    });
    