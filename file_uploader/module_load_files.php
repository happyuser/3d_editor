    <link rel="stylesheet" href="file_uploader/css/styles.css" type="text/css">
    <style type="text/css">
        #info {
                color:#000;
                position: absolute;
                top: 0px; width: 100%;
                padding: 5px;

            }

    </style>
    <script>
    function load_user_file(e, o)
    {
    	e.preventDefault();
        var name = o.attr('name');
        console.log('loading scene with user file: '+name);
        lattice_id = "";
        //var load_file_name=name;
        //var file_type = 'stl';
        //reload_scene();
        $('.main_container').load('canvas_page.php?'+(name?'load='+name:""));
    }
    
	$('.add_sample_3d').on('click',function(e){
		load_user_file(e, $(this));
	});
	
	</script>

    <div class="containerLoader">
        <!-- Button to select & upload files -->
         <span class="btn btn-success fileinput-button"><span>Select files...</span> <!-- The file input field used as target for the file upload widget -->
         <input id="fileupload" type="file" name="files[]" multiple></span> <!-- The global progress bar -->

        <p>Upload progress</p>

        <div id="progress" class="progress progress-success progress-striped bar"></div><!-- The list of files uploaded -->

        <p>Files uploaded:</p>

        <ul id="files"></ul>
        
        <br />You can try preloaded files while uploading is going to be fixed: <br />
        <a name="cube.stl" class='add_sample_3d'> cube |</a>
        <a name="cone.stl" class='add_sample_3d' href="canvas_page.php?load=cone.stl&x_lattices_count=2" target='blank'> cone |</a>
        <a name="torus.stl" class='add_sample_3d' href="canvas_page.php?load=torus.stl&x_lattices_count=2" target='blank'> torus |</a>
        <a name="cilinder.stl" class='add_sample_3d' href="canvas_page.php?load=cilinder.stl&x_lattices_count=2" target='blank'> cilinder |</a>
        <a name="monkey.stl" class='add_sample_3d' href="canvas_page.php?load=monkey.stl&x_lattices_count=2" target='blank'> monkey ;) |</a>
        
    </div>
    <script src="./file_uploader/js/jquery.ui.widget.js" type="text/javascript">
</script><script src="./file_uploader/js/jquery.iframe-transport.js" type="text/javascript">
</script><script src="./file_uploader/js/jquery.fileupload.js" type="text/javascript">
</script><script src="./file_uploader/js/load_files.js" type="text/javascript">
</script>
