		if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

		var container;

		var camera, controls, scene, renderer, mesh;

		var cross;
		var structure = [];
		var str1 = [];

		//var raycaster = new THREE.Raycaster();
		var mouse = new THREE.Vector2(),
		offset = new THREE.Vector3(),
		INTERSECTED, SELECTED;

		var objects = [], plane;
		var containerWidth;
		var containerHeight;

		init();
        
        animate();

		

		// function onDocumentMouseMove( event ) {

		// 	event.preventDefault();

		// 	mouse.x = ( event.clientX / window.innerWidth) * 2 - 1;
		// 	mouse.y = - ( event.clientY / (window.innerHeight + 50) ) * 2 + 1;

		// 	//

		// 	raycaster.setFromCamera( mouse, camera );

		// 	if ( SELECTED ) {

		// 		var intersects = raycaster.intersectObject( plane );

		// 		if ( intersects.length > 0 ) {

		// 			SELECTED.position.copy( intersects[ 0 ].point.sub( offset ) );

		// 		}

		// 		return;

		// 	}

		// 	var intersects = raycaster.intersectObjects( objects );

		// 	if ( intersects.length > 0 ) {

		// 		if ( INTERSECTED != intersects[ 0 ].object ) {

		// 			if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );

		// 			INTERSECTED = intersects[ 0 ].object;
		// 			INTERSECTED.currentHex = INTERSECTED.material.color.getHex();

		// 			plane.position.copy( INTERSECTED.position );
		// 			plane.lookAt( camera.position );

		// 		}

		// 		container.style.cursor = 'pointer';

		// 	} else {

		// 		if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );

		// 		INTERSECTED = null;

		// 		container.style.cursor = 'auto';

		// 	}

		// }

		// function onDocumentMouseDown( event ) {

		// 	event.preventDefault();

		// 	raycaster.setFromCamera( mouse, camera );

		// 	var intersects = raycaster.intersectObjects( objects );

		// 	if ( intersects.length > 0 ) {

		// 		console.log(intersects[ 0 ].object.id);

		// 		controls.enabled = false;

		// 		SELECTED = intersects[ 0 ].object;

		// 		var intersects = raycaster.intersectObject( plane );

		// 		if ( intersects.length > 0 ) {

		// 			offset.copy( intersects[ 0 ].point ).sub( plane.position );

		// 		}

		// 		container.style.cursor = 'move';

		// 	}

		// }

		// function onDocumentMouseUp( event ) {

		// 	event.preventDefault();

		// 	controls.enabled = true;

		// 	if ( INTERSECTED ) {

		// 		plane.position.copy( INTERSECTED.position );

		// 		SELECTED = null;

		// 	}

		// 	container.style.cursor = 'auto';

		// }

		function animate_camera() {

				var tween = new TWEEN.Tween(camera.position.z).to( 10, 3000).easing(TWEEN.Easing.Quadratic.InOut).onUpdate(function () {
						camera.lookAt(scene.position);
				}).onComplete(function () {
						camera.lookAt(scene.position);
				}).start();
		}

		function onWindowResize() {

			camera.aspect = containerWidth / containerHeight;
			camera.updateProjectionMatrix();

			renderer.setSize( containerWidth, containerHeight );

			controls.handleResize();

			render();

		}

		function animate() {

			requestAnimationFrame( animate );
			render();
			controls.update();

		}

		function render() {
			renderer.render( scene, camera );
		}

		var color;
		//var color1 = window.getComputedStyle(document.getElementById("material1"),null).backgroundColor;
		//var color2 = window.getComputedStyle(document.getElementById("material2"),null).backgroundColor;
		//var color3 = window.getComputedStyle(document.getElementById("material3"),null).backgroundColor;
 /*
		function allowDrop(ev) {
		    ev.preventDefault();
		}

		function drag(ev) {
			console.log(ev);
		    ev.dataTransfer.setData("text", ev.target.id);
		    if (ev.currentTarget.id === 'material1') {
		    	color = color1;
		    } 
		    if (ev.currentTarget.id === 'material2') {
		    	color = color2;
		    } 
		    if (ev.currentTarget.id === 'material3') {
		    	color = color3;
		    } 
		}

		function drop(ev) {
			latice_filename =  ev.dataTransfer.getData("text"); //module.stl
			init();
		    ev.preventDefault();
		    var data = ev.dataTransfer.getData("text");
		    objects[0].material.color.setRGB(color.match(/\d+/g)[0], color.match(/\d+/g)[1], color.match(/\d+/g)[2]);
		    // ev.target.appendChild(document.getElementById(data));
		}
		*/
		 
		function buildAxes( length ) {
		var axes = new THREE.Object3D();

		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( length, 0, 0 ), 0xFF0000, false ) ); // +X
		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( -length, 0, 0 ), 0xFF0000, true) ); // -X
		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, length, 0 ), 0x00FF00, false ) ); // +Y
		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, -length, 0 ), 0x00FF00, true ) ); // -Y
		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, length ), 0x0000FF, false ) ); // +Z
		axes.add( buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, -length ), 0x0000FF, true ) ); // -Z

		return axes;

	}

	function buildAxis( src, dst, colorHex, dashed ) {
		var geom = new THREE.Geometry(),
			mat; 

		if(dashed) {
			mat = new THREE.LineDashedMaterial({ linewidth: 3, color: colorHex, dashSize: 3, gapSize: 3 });
		} else {
			mat = new THREE.LineBasicMaterial({ linewidth: 3, color: colorHex });
		}

		geom.vertices.push( src.clone() );
		geom.vertices.push( dst.clone() );
		geom.computeLineDistances(); // This one is SUPER important, otherwise dashed lines will appear as simple plain lines

		var axis = new THREE.Line( geom, mat, THREE.LinePieces );

		return axis;

	}





















