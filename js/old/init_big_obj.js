var scene;
var controls;

var newMesh;

function resize_canvas()
{
	$("#container canvas").width('100%');
	$("#container canvas").height($("#container").width()*window.innerHeight/window.innerWidth );
	$("#container canvas").mouseout(function (){
		controls.enabled = false;
		//console.log('controls.enabled = false;');
	});//
	$("#container canvas").mouseover(function (){
		controls.enabled = true;
		//console.log('controls.enabled = true;');
	});
}

$(document).ready(function(){
	 	    resize_canvas();
});



	

function scale_to500_and_move_to0(mesh, scale_to_size)
{
	
	var u_box = new THREE.Box3().setFromObject( mesh );
	//console.log( box.min, box.max, box.size() );
	var u_size = u_box.size();
	
	mesh.scale.set(scale_to_size/u_size.x, scale_to_size/u_size.x, scale_to_size/u_size.x);
	
	mesh =  move_to_0(mesh);
	return mesh;

}

function move_to_0(mesh)
{
		
	u_box = new THREE.Box3().setFromObject( mesh );
	
	return  move_to_0_box(mesh, u_box);
	
}

function move_to_0_box(mesh, box)
{
		p = new THREE.Vector3(-(u_box.min.x + u_box.max.x)/2, -(u_box.min.y + u_box.max.y)/2, -(u_box.min.z + u_box.max.z)/2);
	
	mesh.position = p; // =  u_bounds.position.negate()
	return mesh;

}

function show_bounds_box(u_size, scene)
{
				//Show bounds of the model
	u_bounds = new THREE.Mesh( new THREE.BoxGeometry( u_size.x, u_size.y, u_size.z ), m_Metal_tr );
	mesh.position.copy(box.min.add(box.max));

	u_bounds.position.x = (box.min.x + box.max.x)/2;
	u_bounds.position.y = (box.min.y + box.max.y)/2;
	u_bounds.position.z = (box.min.z + box.max.z)/2;
	scene.add(u_bounds);
	return scene;
}



function init() {
	
	var bmap =  THREE.ImageUtils.loadTexture("img/bump.png", {}, function(){});
var smap =  THREE.ImageUtils.loadTexture("img/specular_map.jpg", {}, function(){});

//***************** materials

var mLattice_normal = new THREE.MeshNormalMaterial( { color: 0x22ff22, overdraw: 0.2, opacity: 0.6,  transparent: true, depthTest: true,
depthWrite: false,
polygonOffset: true,
polygonOffsetFactor: 5
  });

//var materialPhong = new THREE.MeshPhongMaterial({color: 0xFF0000});
//var material = new THREE.MeshLambertMaterial( { color: 0x000000, opacity: 0.4,  transparent: true } );//side: THREE.DoubleSide, wireframe: true
//alert(pos); 
var materialTransparent = new THREE.MeshBasicMaterial( { color: 0x22ff22, overdraw: 0.2, opacity: 0.6,  transparent: true  } );//, side: THREE.DoubleSide 

var materialTransparent_white = new THREE.MeshBasicMaterial( { color: 0x999999,  opacity: 0.4,  transparent: true  } );//, side: THREE.DoubleSide,overdraw: 0.2,
var materialNormal = new THREE.MeshNormalMaterial({ color: 0x000000, side: THREE.DoubleSide, transparent:true, opacity: 0.3,   
 depthTest: true,
depthWrite: false,
polygonOffset: true,
polygonOffsetFactor: 5

});

var mGlass = new THREE.MeshLambertMaterial( {
color: 0xaaaa88,
//envMap: reflectionCube,
opacity: 0.2,
transparent: true
} );
	/* Old Wall */
var mLattice_red = new THREE.MeshPhongMaterial({
color      :  new THREE.Color(0xaa0000),//"rgb(155,196,30)"),
emissive   :  new THREE.Color("rgb(7,3,5)"),
specular   :  new THREE.Color("rgb(255,113,0)"),
transparent:true,  opacity: 0.6 ,
shininess  :  20,
bumpMap    :  bmap,
map        :  smap,
bumpScale  :  10,

depthTest: true,
depthWrite: false,
polygonOffset: true,
polygonOffsetFactor: 5
//
});
	
	var m_Metal_tr = new THREE.MeshPhongMaterial( {
ambient: 0x030303,
color: 0xdddddd,
specular: 0xFFEE7C,
shininess: 10,
shading: THREE.FlatShading,
//overdraw: 0.2,
opacity: 0.6,
metal: true,
transparent: true,
});

var materialSH =  new THREE.MeshLambertMaterial( { color:0xffffff, shading: THREE.FlatShading } );
				
				//***************** end of materials

	

				THREE.crossOrigin = "";
				//var fill_user_model_with_lattices;
				count_down_show();
	
				camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
				camera.position.z = 200;
				
				controls = new THREE.TrackballControls( camera );

				
				controls.rotateSpeed = 1.0;
				controls.zoomSpeed = 0.8;
				controls.minDistance = 80;
				controls.maxDistance = 700;
				controls.panSpeed = 0.8;

				controls.noZoom = false;
				controls.noPan = true;

				controls.staticMoving = false;
				controls.dynamicDampingFactor = 0.3;

				controls.keys = [ 65, 83, 68 ];

				controls.addEventListener( 'change', render );

				//controls.object.position =  new THREE.Vector3(271.4963301460587, 266.84072956617155, -525.1061129272566);
				controls.enabled = false; // wait until on mouse move
				
				/* for f1_back version
				controls.object.position.x = 5.791697275117267;
				controls.object.position.y = -62.04883134635663;
				controls.object.position.z = 22.558857016688865;
				*/

				// world

				scene = new THREE.Scene();
				
				// Add axes
				axes = buildAxes( 1000 );
				scene.add( axes );

				scenecolor = 0xcccccc; 
				//scene.fog = new THREE.FogExp2( 0xcccccc, 0.001 ); // ������ ����� �� �����
                var ambient = new THREE.AmbientLight( 0xffbbbb );
				scene.add( ambient );

				var directionalLight = new THREE.DirectionalLight( 0xffeedd ); // ������ ��������� �������
				directionalLight.position.set( 0, 0, 1 ).normalize();
//				scene.add( directionalLight );
				
				light = new THREE.DirectionalLight( 0xccffff );
				light.position.set( 1, 1, 1 );
//				scene.add( light );


				light = new THREE.DirectionalLight( 0x888888 );
				light.position.set( -1, -1, -1 );
//				scene.add( light );

								// lights
				var light = new THREE.HemisphereLight(0xfffff0, 0x101020, 1);
			    light.position.set(0.75, 1, 0.25);
			    scene.add(light);
				

				
				light = new THREE.AmbientLight( 0x222222 );
				light.position.set( -1, -1, -1 );
				//scene.add( light );
				
				var spotLight = new THREE.SpotLight( 0xffffff ); 
				    spotLight.position.set( -250, 250, 250 );  
				    spotLight.castShadow = true;  
				    spotLight.shadowMapWidth = 1024; 
				    spotLight.shadowMapHeight = 1024;  
				    spotLight.shadowCameraNear = 500; 
				    spotLight.shadowCameraFar = 4000; 
				    spotLight.shadowCameraFov = 30; 
    			scene.add( spotLight );
            
				// texture
				var opts = {
					color: '#000'
				} 	
				var target = document.getElementById('progress');
				var spinner = new Spinner(opts).spin(target);

				THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {
				     var progress = 100/total*loaded;
						if (loaded == total) {
							console.log('file loaded: '+load_file_name);
							document.getElementById('progress').style.display = "none";
						}

				};

				// model
				console.log('loading user uploaded file: '+load_file_name);
				console.log('with file type: ' + file_type);
				var user_geometry;
				
				
				if(load_file_name != '' && file_type != '' && (file_type == 'stl' || file_type == 'STL'))
				{
					count_down_show();
					var user_loader = new THREE.STLLoader();
					user_loader.load( './files/'+load_file_name, function ( geometry ) {
						user_geometry = geometry;
						
						user_mesh = new THREE.Mesh(user_geometry, m_Metal_tr);//materialPhong, materialNormal, materialTransparent_white
	
	
	console.log('user_geometry: ' + user_geometry);
			
		
	//var latice_filename = "test"; //without ext
	//soccer ball, shell_nte3, 2.obj, optimised2, couple_paralepipeds, test, box_lattice_section, module
	$('#count_down').html('loading file ...');
	
	var o1;
	
	//				var loader = new THREE.OBJLoader( );
	//				loader.load( 'obj/'+latice_filename+'.obj', function ( object ) {
	//
	
	//				var loader = new THREE.OBJMTLLoader(); //������ ���������� three_66.js 
	//					loader.load( 'obj/'+latice_filename+'.obj', 'obj/'+latice_filename+'.mtl', function ( object ) {
	
	//************************************************************************************
	
	if(lattice_file_name != "")
	{;//work with lattice set from GET param
	}
	else
	{
		if(lattice_id != "" && (typeof lattice_id !== 'undefined'))
		{
			switch(lattice_id) {
		  case 'el_5':  // if (x === 'value1')
		    lattice_file_name = 'unitCell.stl';
		    break
		
		  case 'el_6':  // if (x === 'value2')
		    	lattice_file_name = "simple2a.stl";
		    break
		
		case 'el_7':  // if (x === 'value2')
		    	lattice_file_name = "simple1a.stl";
		    break
		case 'el_8':  // if (x === 'value2')
		    	lattice_file_name = "simple3a.stl";
		    break
		case 'el_9':  // if (x === 'value2')
		    	lattice_file_name = "module.stl";
		    break
		
		  default:
		    //lattice_file_name = "module.stl";
		    break;
		    }
		}
		else
		{
			//user_mesh = new THREE.Mesh(user_geometry, materialNormal);//materialPhong, materialNormal, materialTransparent_white, mLattice_normal
			//user_mesh.scale.set(3,3,3);
			//user_mesh = move_to_0(user_mesh);
			
			scene.add(scale_to500_and_move_to0(user_mesh, 100));
			console.log("No lattice specified - skipping adding lattice; ");
			// ***********************************
			resize_canvas();
			count_down_hide();
			return;
			//lattice_file_name = "module.stl";
		}
		
	}
	console.log('lattice_file_name:'+lattice_file_name);
	count_down_show();
	$('#count_down').html('creating scene ...');
		
	
	var loader = new THREE.STLLoader();
	loader.load( './stl/lattice/'+lattice_file_name, function ( geometry ) {
		
	//TODO: for development - preview lattices
	//var l_mesh =  new THREE.Mesh( geometry, materialN_l );
	//l_mesh.scale.set(40,40,40);
	// = 0;
	//l_mesh.position.x = 0;
//	l_mesh.position.y = 0;
//	l_mesh.position.z = 0;
//	scene.add(l_mesh);
//	count_down_hide();
//	return;
	 
			var mesh = new Array();
	//var material = new Array();
	var m = 1;
	var pos;
	
	var mesh = new THREE.Mesh(geometry);
	var b_l = new THREE.Box3().setFromObject( mesh ); //box_lattice
	//console.log( b_l.min, b_l.max, b_l.size() );
	//return;
	
	var big_geometry = new THREE.Geometry();
	user_mesh = new THREE.Mesh(user_geometry, materialTransparent_white);
	//scene.add(user_mesh);
	
	var box = new THREE.Box3().setFromObject( user_mesh );
	//console.log( box.min, box.max, box.size() );
	var u_size = box.size();
	var u_model_s_v = 500/u_size.x;
	
	
	// number of lattices in user object bounding box - axis x
	var sc = 1; // 6 in a row is maximum
	if(x_lattices_count != "")
	{
		sc = x_lattices_count;
		console.log('got lattices count from form: '+x_lattices_count);
	}	
	console.log('x_lattices_count: '+x_lattices_count);
	//return;
	var l_scale = 1/sc; // one lattice size / user object size
	
	//get needed size of lattice on x vertice
	var c_x = u_size.x/sc;
	
	//get lattice geometry  scale ratio (geometry got from file - 58px to ?)
	var l_file_scale = c_x / b_l.size().x;
	
	//sizes of lattice  
	var c_y = b_l.size().y*l_file_scale ;
	var c_z = b_l.size().y*l_file_scale ;
	
	//get latices count on vertices
	var x_length = Math.ceil(u_size.x/c_x);
	var y_width = Math.ceil(u_size.y/c_y);
	var z_height = Math.ceil(u_size.z/c_z);
	
	var faces_count = geometry.faces.length*z_height*y_width*x_length;
	//console.log('user_geometry.faces.length'+user_geometry.faces.length);
		
	//start count down for intersaction process
	console.log('faces count in big_geometry is: '+faces_count);
	var time_amount_0 = 56; //91s for 17972 obj file faces , 56s for 5364 stl file 
	var faces_count_0 = 5364+1200;
	var time_left = Math.ceil(faces_count / faces_count_0 * time_amount_0);
	start_count_down(time_left, true);
	
	//get new object to make intersaction 
	var objectBSP;
	var b_l_s = new THREE.Box3();; //box_lattice_scaled 
	
	//create lattices frame
	for(z = 0; z<z_height; z++)
	{
		for(y = 0; y<y_width; y++)
		{
			for(pos = 0; pos<x_length; pos++)
			{
				mesh[pos] = new THREE.Mesh( geometry );
				mesh[pos].scale.set(l_file_scale, l_file_scale, l_file_scale); //scale the lattice geometry
				if(pos == 0) 
					b_l_s = new THREE.Box3().setFromObject( mesh[pos] );
				
				mesh[pos].position.x = box.min.x - b_l_s.min.x + pos*(b_l_s.size().x);
				mesh[pos].position.y = box.min.y - b_l_s.min.y + y*(b_l_s.size().y);
				mesh[pos].position.z = box.min.z - b_l_s.min.z + z*(b_l_s.size().z);
				
				//merge all lattices together for beter rendering speed. 
				THREE.GeometryUtils.merge(big_geometry, mesh[pos]);
				
				//TODO: make connection a manifold2 - for 3d printing
			}//pos
					
		}//y
	}//z
	
	//***************************************************
	//to fill gaps that will left after intersaction
	//THREE.GeometryUtils.merge(big_geometry, user_mesh); //not needed now - replaced with add object.
	//        
	//put merged latice material to scene
	big_geometry.dynamic = true;
	var big_mesh = new THREE.Mesh(big_geometry, materialNormal);
	//scene.add(user_mesh);
	//scene.add(big_mesh);
	//return;
	//*******\

	// merged lattices intersect Sphere
	objectBSP = new ThreeBSP( big_mesh);
	userBSP = new ThreeBSP( user_mesh);
	
	//***** long operation !!!!!!!!!
	console.log('!!!! creating sphereBSP.intersect( objectBSP )');
	var timerStart = Date.now();
	var newBSP = objectBSP.intersect( userBSP); // objectBSP  cubeBSP, userBSP, sphereBSP
	var time_used = Math.ceil((Date.now()-timerStart)/1000);
	console.log("Time used for intersection: ", time_used);
	
	//Create new mesh from that intersected object ****************
	var timerStart = Date.now();
	console.log('creating newMesh = newBSP.toMesh( materialTransparent )');
	newMesh = newBSP.toMesh( mLattice_red); //mLattice_red, mLattice_normal, oldMaterial, materialPhong, materialNormal, materialTransparent_white, materialTransparent

//	scene.add(user_mesh);
//	return; 
	console.log("Time until intersect: ", Math.ceil((Date.now()-timerStart)/1000));
	console.log('-u_size.x/2:'+-u_size.x/2);
	//get user model scale to best view;
	newMesh.scale.set(u_model_s_v,u_model_s_v,u_model_s_v);
	//**************************
	
	user_mesh = new THREE.Mesh(user_geometry, mGlass);//materialPhong, materialNormal, materialTransparent_white
	user_mesh.scale.set(u_model_s_v, u_model_s_v , u_model_s_v );
	
	user_m_cover = new THREE.Mesh(user_geometry , m_Metal_tr );//, materialNormal,  materialPhong
	sc_m_cover = u_model_s_v*1.05;
	user_m_cover.scale.set(sc_m_cover, sc_m_cover , sc_m_cover );
	
	///////////////////////
	////////////////////////
	//move lattices box according to user mesh b                                                                   		//scale to proper size and translate objects
	u_box = new THREE.Box3().setFromObject( user_mesh );
	user_mesh = move_to_0(user_mesh);
	newMesh = move_to_0_box(newMesh, u_box );
	user_m_cover = move_to_0(user_m_cover);
	 
	//show objects
	scene.add( newMesh );
	scene.add(user_mesh);	
	scene.add(user_m_cover); 
	
	$('#count_down').html('Done, in '+ time_used +' seconds');
	setTimeout(function() {$('#count_down').hide("slow");}, 5000);
	} );//end of User OBJloader
	
	
});//user_loader.load( './files/'+load_file_name
					
				}//if(load_file_name != ''
                
				count_down_hide();

				// renderer

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setClearColor( scenecolor, 1 ); // Fog //scene.fog.color
				renderer.setSize( window.innerWidth, window.innerHeight );

				container = document.getElementById( 'container' );
				container.appendChild( renderer.domElement );
				resize_canvas();
				
				/*stats = new Stats();
				stats.domElement.style.position = 'absolute';
				stats.domElement.style.top = '0px';
				stats.domElement.style.zIndex = 100;
				container.appendChild( stats.domElement );
                */
				//

				window.addEventListener( 'resize', onWindowResize, false );
				
				

			}

