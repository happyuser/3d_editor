		var youngMod = $('#youngMod');
		var posRat = $('#posRat');
		var den = $('#den');
		var thermalExp = $('#thermalExp');
		var valFrac = $('#valFrac');
		var material_num = 0;
		var lattice_id  = "";
		var x_lattices_count =1;

		


function clear_scene()
{
	var l = scene.children.length;

    //remove everything
    while (l--) {

        //if(scene.children[l] instanceof THREE.Camera) continue; //leave camera in the scene

        scene.remove(scene.children[l]);

    }
	$('canvas:nth-of-type(1)').remove();	
}

function reload_scene() {
	
    
    //reinitialise your stuff
    init();

}


function drop(ev) {
	var main_container= $('.main_container');
    ev.preventDefault();
    lattice_id = ev.dataTransfer.getData("text");
    console.log('droped lattice with id: '+lattice_id);
    x_lattices_count = $('#lattices_count').val();
    console.log('lattices_count set to: ' + x_lattices_count);
    name = load_file_name;
    count_down_show();
    main_container.load('canvas_page.php?'+(name?'load='+name:""));
}
function allowDrop(ev) {
    ev.preventDefault();   
}

function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
				    
	//console.log(ev);
}
		
function enterATMmateralValue(){
		if($('#sel_materal').val() == 'Steel'){	
			youngMod.val(210000);
			posRat.val(0.3);
			den.val('7.9e9');
			thermalExp.val(17.3);
			valFrac.val(0.3);
		}
		else if($('#sel_materal').val() == 'Aluminum'){	
			youngMod.val(70000);
			posRat.val(0.3);
			den.val('2.7e9');
			thermalExp.val(22.7);
			valFrac.val(0.3);
		}
		else{
			youngMod.val('');
			posRat.val('');
			den.val('');
			thermalExp.val('');
			valFrac.val('');			
		}
}
$(function(){
			//TODO: for testing
		var i=0; $('#basic_prop_3d .checkbox input').each(function(){
				if(i>4)
					$('<li draggable="true" ondragstart="drag(event)" id="el_'+i+'">'+$(this).next('span').html()+'</li>').appendTo('.latProp ul');
					$(this).removeAttr("checked");
					i++;
				});		
//		 end for testing

	
	$('#lattices_count').on('change', function (){
		var max_lattices_count = 5; //for javascript to compute intersaction of cube with a simplest lattice simple3a.stl
		if($(this).val()>3)
		{
			w = 'JavaScript can fall on computing such a big amount of lattice on the scene. And for shure will do if lattice or user object is complicated';
			alert('Warning. '+w);
			//print_warning(w);
		}
		if($(this).val()>max_lattices_count)
		{
			alert('Sory. But Java script can not compute such a big amount of lattice on the scene');
			$(this).val(max_lattices_count);
		}
	});
		
	$('#sel_materal').on('change',function(){

		enterATMmateralValue()
		
	});
	
	$('#select_btn').on('click',function(){
		material_num ++;
		var li = $('.materialProp ul').find('li');
		$('.select_block').show(1000);
		/*$('.material input, .material select').each(function(){			
				$('<li>'+$(this).val()+'</li>').appendTo('.materialProp ul');
				$(this).val('')			
		});*/
		if($('.material input#name').val() != ''){
			$('<li>'+$('.material input#name').val()+'</li>').appendTo('.materialProp ul');	
			$('<option>'+$('.material input#name').val()+'</option>').appendTo('.selectMaterial2d'); //append option to advancen properties 2d select materials	
		}
		else{
			$('.material input#name').val('Material '+material_num)
			$('<li>'+$('.material input#name').val()+'</li>').appendTo('.materialProp ul');	
			$('<option>'+$('.material input#name').val()+'</option>').appendTo('.selectMaterial2d'); //append option to advancen properties 2d select materials	
		}
		
		metals.push({
			sel_matal: $('#sel_materal').val(), 
			name: $('.material input#name').val(),
			youngMod: youngMod.val(),
			posRat: posRat.val(),
			den: den.val(),
			thermalExp: thermalExp.val(),
			valFrac: valFrac.val()
		})	
		//console.log(metals)
		
		$('.material input, .material select').each(function(){
				$(this).val('')		
		});

	});

	$(document).on('click','.materialProp ul li', function(){
		//console.log($(this).text());
		var text = $(this).text();
		for(var i=0; i<metals.length; i++){
			if(metals[i].name == text){
				$('#sel_materal').val(metals[i].sel_matal), 
				$('.material input#name').val(metals[i].name),			
				youngMod.val(metals[i].youngMod);
				posRat.val(metals[i].posRat);
				den.val(metals[i].den);
				thermalExp.val(metals[i].thermalExp);
				valFrac.val(metals[i].valFrac);
			}
		}
		$('#sel_materal').on('change',function(){
			$('.material input, .material select').each(function(){
					$('.material input#name').val('')
					enterATMmateralValue();
				
			});		
		});
	});		

	
	$('.close_open').on('click',function(){
		//$(this).parents('.select_block .box-content').animate({'height': '+=100%'}, 2000, 'swing');
		var this_btn = $(this);
		if(this_btn.parents('.select_block .box-content').height() > '0'){
			this_btn.parents('.select_block .box-content').css('height', '0');	
			this_btn.find('i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');			
		}
		else{
			this_btn.parents('.select_block .box-content').css('height', '100%');
			this_btn.find('i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
		}
	});
	$('.close_open2').on('click',function(){
		if($(this).next('.box-content').height() > '0'){
			$(this).next('.box-content').css('height', '0');
		}
		else{
			$(this).next('.box-content').css('height', '100%');
		}
	});
	
	$('#design_btn').on('click',function(){
		$('.video_img_block').show(1000);
	})
	
	var num = 1;
	var name = 'Custom '+ num;
	$('#play_video').on('click',function(){
		$('#video1').get(0).play();
		
		var customName = $('#customName').val();
		if(customName){
			name = customName;
		}
		else{
			name = 'Custom '+ num;
		}
		
	});
	$('#video1').on('ended', function()	{
			num++;
			var poster = $('#video1').attr('poster');
			//$('.advansedLatProp ul li').remove();
			$('<li><input type="checkbox" /><label>'+name+'<img src='+poster+' width="30px" ></label></li>').appendTo('.advansedLatProp ul');
			$('.video_img_block').hide(1000);
			$('#advanced_prop_btn').show();
			$('.video_ended_block').show();
	});
	
	$('#close').on('click',function(){
		$('.video_ended_block').hide();
	});

	$('#checkbox_3d').on('click', function(){
		if($(this).is(':checked')){
			$('#basic_prop_2d').hide();
			$('#basic_prop_3d').show();
		}
		else{
			$('#basic_prop_3d').hide();
			$('#basic_prop_2d').show();			
		}
	})
	
	$('#checkbox_3d_Advanced').on('click', function(){
		if($(this).is(':checked')){
			$('.ad_prop_2d').hide();
			$('.ad_prop_3d').show();
		}
		else{
			$('.ad_prop_3d').hide();
			$('.ad_prop_2d').show();			
		}
	})	
	
	$('#basic_prop_btn').on('click',function(){
		//var li = $('.latProp ul').find('li');
		var i=0;
		$('#basic_prop_2d .checkbox input').each(function(){
		if($(this).is(':checked'))// && (li.text() == ''))
			$('<li draggable="true" ondragstart="drag(event)" id="el_'+i+'">'+$(this).next('span').html()+'</li>').appendTo('.latProp ul');
			// i ���� ���� ������� �� $(this).attr('id') ���� � ������ ������ �� ����� design_lattice.html
			$(this).removeAttr("checked");
			i++;
		});		
	});
	$('#basic_prop_btn_3d').on('click',function(){
		//var li = $('.latProp ul').find('li');
		var i=0;
		$('#basic_prop_3d .checkbox input').each(function(){
		if($(this).is(':checked') )//&& (li.text() == ''))
			$('<li draggable="true" ondragstart="drag(event)" id="el_'+i+'">'+$(this).next('span').html()+'</li>').appendTo('.latProp ul');
			$(this).removeAttr("checked");
			i++;
		});		
		
		$('.latProp ul li').each(function(){
			$(this).on('click',function(){
				
			});
		});
	});	

	
	
	$('#advanced_prop_btn').on('click',function(){
		/*var posterImg= $(this).prev('ul').html();
			//console.log(posterImg);
			$(posterImg).appendTo('.latProp ul');	
			$(this).prev('ul').children('li').remove();
			$('#advanced_prop_btn').hide();			
			*/
			$('.advansedLatProp ul li input').each(function(){
			if($(this).is(':checked') )//&& (li.text() == ''))
				$('<li>'+$(this).next('label').html()+'</li>').appendTo('.latProp ul');
				$(this).removeAttr("checked");
			});			
	});

		
	$('#basic_prop_2d img').hover(function(){
		var thisInfo = $(this).parents('.checkbox').find('.info2d')
		if(thisInfo){
		openBlock(thisInfo);
		}
	},function(){
		var thisInfoBlock = $('.hover_2d').find('.info2d')
		closeBlock(thisInfoBlock);
	}
	);
});
function openBlock(selector) {
	selector
		.clone()
		.appendTo('.hover_2d')
		.show()
		.parent()
		.fadeIn('fast');
}
function closeBlock(selector) {
	selector
	.parents('.hover_2d')
	.fadeOut('fast', function() {
		$(this)
			.find('.info2d')
			.remove();
	});
}
$(function(){
	$('#m_valFrac2d, #m_valFrac2d2').val(0.3)
	//$(document).bind('textchange','#m_valFrac2d', function(){
	$('#m_valFrac2d').on('change', function(){	
		$('.warning').hide();
		var val = $('#m_valFrac2d').val()
		if(val > 1 || val <= 0){
			$('.warning').show();
		}
	})
	$('#m_valFrac2d2').on('change', function(){	
		$('.warning2').hide();
		var val = $('#m_valFrac2d2').val()
		if(val > 1 || val <= 0){
			$('.warning2').show();
		}
	});
	$('#numOfM2').on('change', function(){
		if($('#numOfM2').val() == '2'){
			$('.onlyFor2M').show();
		}
		else{
			$('.onlyFor2M').hide();			
		}
	});
});