
function load_couple_lattices()
{
	
	var loader = new THREE.STLLoader();

	loader.load( './stl/module.stl', function ( geometry ) {

		var mesh = new Array();
		var material = new Array();
		var m = 1;
		var pos;
		
		//sizex = $
		
		for(z = 0; z<5; z++)
		{
			for(y = 0; y<2; y++)
			{
				for(pos = 0; pos<2; pos++)
				{
		
					//var material = new THREE.MeshLambertMaterial( { color: 0x000000, opacity: 0.4,  transparent: true } );//side: THREE.DoubleSide, wireframe: true
					//alert(pos); 
					material[pos] = new THREE.MeshLambertMaterial( { color: 0xffffff, overdraw: 0.5, opacity: 0.4,  transparent: true  } ); 
				
					mesh[pos] = new THREE.Mesh( geometry, material[pos] );
					mesh[pos].position.x = pos*60;
					//console.log(mesh[pos].position.x);
					mesh[pos].position.z = z*60;
					mesh[pos].position.y = y*60;
					scene.add( mesh[pos] );
					objects.push( mesh[pos] );
				}
			}
		}
		
	} );
	

}

function init() {

			containerWidth = document.getElementById( 'container' ).offsetWidth;
			containerHeight = document.getElementById( 'container' ).offsetHeight;

			camera = new THREE.PerspectiveCamera( 45, containerWidth / containerHeight, 1, 1000 );
			camera.position.set(0, 0, 500);

			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.setSize( containerWidth, containerHeight );
			document.body.appendChild(renderer.domElement);

			controls = new THREE.OrbitControls( camera, renderer.domElement );
			controls.target.set = new THREE.Vector3(0, 0, 0);
			controls.minDistance = 10;
			controls.maxDistance = 500;
			controls.maxPolarAngle = Math.PI;

			// world

			scene = new THREE.Scene();
			scene.fog = new THREE.FogExp2(0xcce0ff, 0.0003);

			var ambient = new THREE.AmbientLight( 0xcccccc );
			scene.add( ambient );
			
			//load_couple_lattices();
            var opts = {
					color: '#000'
				}

            var target = document.getElementById('progress');
			var spinner = new Spinner(opts).spin(target);

			THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {
			    // var progress = 100/total*loaded;
					if (loaded == total) {
						document.getElementById('progress').style.display = "none";
					}

			};

//return;
			// model
            var materialNormal = new THREE.MeshNormalMaterial({ color: 0x000000, side: THREE.DoubleSide, transparent:true, opacity: 0.4  });
            var material = new THREE.MeshLambertMaterial( { color: 0x000000, side: THREE.DoubleSide, wireframe: true } );
            var materialTransparent_white = new THREE.MeshLambertMaterial( { color: 0x111100 } );
            //, side: THREE.DoubleSide,overdraw: 0.2, overdraw: 0.2,
            //opacity: 0.9
			var loader = new THREE.STLLoader();
            // // ASCII file
			loader.load( './stl/box_lattice_small.stl', function ( geometry ) {
				var mesh = new THREE.Mesh( geometry,materialNormal );
				// mesh.rotateZ(1.53);
				//mesh.position.x = - 70;
//				mesh.position.z = - 70;
//				mesh.position.y = -100;
				scene.add( mesh );
				objects.push( mesh );
                var a = -150;
                var b = 150;
                var c = 50;
                
                light = new THREE.PointLight( 0xffffff, 4, 100 , 2);
                light.position.set(a,b,c);
                scene.add( light );
                
              	var sphereGeometry = new THREE.SphereGeometry( 5, 2, 2 );
                var sphereMesh = new THREE.Mesh( sphereGeometry ,materialTransparent_white);
                sphereMesh.position.set(a,b,c);
                scene.add( sphereMesh );
    

//    return;
				
				// var objectBSP = new ThreeBSP( geometry );
                      
	
	//var cubeGeometry = new THREE.CubeGeometry( 100, 100, 100, 1, 1, 1 );
//	var cubeMesh = new THREE.Mesh( cubeGeometry );
//	var cubeBSP = new ThreeBSP( cubeMesh );
//		
//	var sphereGeometry = new THREE.SphereGeometry( 60, 32, 32 );
//	var sphereMesh = new THREE.Mesh( sphereGeometry );
//	var sphereBSP = new ThreeBSP( sphereMesh );
//	
//	// Example #4 - Cube intersect Sphere
//	var newBSP = sphereBSP.intersect( cubeBSP ); //objectBSP //cubeBSP
//	var newMesh = newBSP.toMesh( materialNormal );
//	newMesh.position.set(-70, 60, -120);
//	scene.add( newMesh );
			} );
            

			// mesh = objects[0];
			// mesh.geometry.boundingSphere.radius = 100;


			// // Binary files

			// var material = new THREE.MeshPhongMaterial( { ambient: 0x050505, color: 0x0033ff, specular: 0x555555, shininess: 30 } );

			// loader.load( './stl/cube.stl', function ( geometry ) {

			// 	var mesh = new THREE.Mesh( geometry, material );


			// 	mesh.castShadow = true;
			// 	mesh.receiveShadow = true;

			// 	// mesh.position.copy( camera.position );
			// 	// mesh.rotation.copy( camera.rotation );
			// 	// mesh.updateMatrix();
			// 	// mesh.translateZ( - 10 );

			// 	scene.add( mesh );
			// 	objects.push( mesh );
			// 	console.log (mesh.id);

			// } );

			//plane = new THREE.Mesh(
//				new THREE.PlaneBufferGeometry( 800, 800, 8, 8 ),
//				new THREE.MeshBasicMaterial( { visible: false } )
//			);
			//scene.add( plane );

			// lights
            
            
			directionalLight = new THREE.DirectionalLight( 0x222222, 5 );
			directionalLight.position.set( 1, 1, 1 );
			scene.add( directionalLight );

			ambientLight = new THREE.AmbientLight( 0x222222 );
			ambientLight.position.set( -1, -1, -1 );
			scene.add( ambientLight );

			// renderer
            
			renderer.setClearColor( scene.fog.color, 1 ); // Fog //scene.fog.color

			container = document.getElementById( 'container' );
			container.appendChild( renderer.domElement );

			// // when the mouse moves, call the given function
			// document.addEventListener( 'mousemove', onDocumentMouseMove, false );
			// document.addEventListener( 'mousedown', onDocumentMouseDown, false );
			// document.addEventListener( 'mouseup', onDocumentMouseUp, false );

			function onWindowResize() {   

	            camera.aspect = containerWidth / containerHeight;
	            camera.updateProjectionMatrix();

	            renderer.setSize( containerWidth, containerHeight );
        	}
		}