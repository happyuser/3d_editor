<?php
//to run only one open scad at a time
$a = array(); $i = 0;
if(array_key_exists('kill_prev', $_GET))
 {exec('pkill -x openscad');
 echo('!! Killed previous OpenScad process to trart this one !!<br/>');
 }
exec('pgrep -x openscad', $a, $i);
if ($i == 0) exit("The openscad program is busy now. Try later.");

header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<html><head><meta charset="utf-8" /><title>Two stl files intersaction</title></head><body>
<pre>
<?php
ini_set('display_errors', '1');
error_reporting(E_ALL);
set_time_limit(0);

function run_openscad($parametrs) {
	$address = gethostbyname('localhost');
	$service_port = 9999;
	$r = '';

	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket === false) exit("Не удалось выполнить socket_create(): причина: " .
		socket_strerror(socket_last_error()) . "\n");

	$result = socket_connect($socket, $address, $service_port);
	if ($result === false)
		exit("Не удалось выполнить socket_connect().\nПричина: ($result) " .
			socket_strerror(socket_last_error($socket)) . "\n");

	if ($s = socket_read($socket, 2048)) { // Читаем ответ.
		if (trim($s) == 'Hello client.') {
			$out =
"password: lWQN9qefCknfdmlF; command: run_openscad; parametrs: $parametrs\r\n";
			socket_write($socket, $out, strlen($out));
			$r = socket_read($socket, 2048);
		} else print "Unknown answer: $s\n";
	} else print "The OpenscadServer did not response.\n";
	socket_close($socket);
	print "The run_openscad command was sent.\n";
	return $r;
}

//if (!chdir('/var/www/html/scad_output'))
	//exit("Can not chdir('/var/www/html/scad_output')");
/*
system(
'openscad -m make -o r4.stl ' .
'-D \'user_file="../scad/cube.stl"\' -D \'lattice="../scad/module.stl"\' ../scad/param.scad 2>&1');
*/
// -D 'l_s_x="58"' -D 'l_s_y="58"' -D 'l_s_z="58"' -D 'u_s_x="100"' -D 'u_s_y="100"' -D 'u_s_z="100"' -D 'l_scale="0.5"'
if (array_key_exists('user_file', $_GET) && array_key_exists('lattice', $_GET)) {
	$user_file = $_GET['user_file'];
	$lattice = $_GET['lattice'];
} else {
	exit("user_file or lattice is not set in \$_GET.");
//	$user_file = "cube.stl";
//	$lattice = "module.stl";
}

///////// copy user file to local drive

 function download_file ($url, $path) {

  $newfilename = $path;
  $file = fopen ($url, "rb");
  if ($file) {
    $newfile = fopen ($newfilename, "wb");

    if ($newfile)
    while(!feof($file)) {
      fwrite($newfile, fread($file, 1024 * 8 ), 1024 * 8 );
    }
  }

  if ($file) {
    fclose($file);
  }
  if ($newfile) {
    fclose($newfile);
  }
 }
 //http://172.31.170.27:82/3d_editor/files/cube.stl
 $user_file_name = basename($user_file); //TODO: do for not stl
 $local_file_path = "files/".$user_file_name;//cube.stl
 $remote_path = dirname($user_file);
 
 //create local folder if it does not exist
 if(!file_exists(dirname( $local_file_path))) 
    mkdir(dirname( $local_file_path));

 //print '$user_file:'.$user_file;
//print '$remote_path:'.$remote_path;
 //exit;
 if($remote_path != ".") {
	download_file ($user_file, $local_file_path);
	$user_file = $user_file_name; //TODO: !!! reload filename - from remote link to local
 }
 else 
 {
	 //no changes
 }
 
 //exit;
/////////

$mode = (array_key_exists('preview', $_GET) ? 'preview' : 'production');

// Перевірка присутності параметрів та ствлрення стрічки з додатковиии параметрами для openscad:
$parametrsNames='l_s_x,l_s_y,l_s_z,u_s_x,u_s_y,u_s_z,l_scale'; $s = '';
foreach (explode(',', $parametrsNames) as $v) {
	if (!isset($_GET[$v])) exit("$v is not given.");
	if (!empty($s)) $s .= ' ';
	$s .= "-D '" . $v . "=\"" . $_GET[$v] . "\"'";
}

$time = microtime(true); // time in Microseconds
if ($mode == 'production') $output_file_path = "../scad_output/r_php.stl";
else { // preview mode
	$a = explode('/', $user_file);
	$user_file_name = $a[count($a) - 1];
	$preview_file_name = preg_replace('/\.[^.]+$/', '', $user_file_name) .
		'_' . time() . '.png';
	$output_file_path = '../scad_output/' . $preview_file_name;
}

$parametrs = '-m make '.
	(($mode == 'preview') ? '--preview ' : '') .
	' --viewall --camera=0,0,0,30,30,30,100 '.
	'-o ' . $output_file_path . ' -D \'user_file="../files/' . $user_file .
	'"\' -D \'lattice="../stl/lattice/' . $lattice . '"\' ' . $s . ' ../scad/3d_editor.scad';
$command = 'openscad '. $parametrs;

print "Command:<br/><textarea style='width: 96%' rows=5>$command</textarea><br/>";

//get file from a remote server


$happyusers_office_ip = "172.31.170.27";
$result = array();
if ($mode == 'preview') {
	$r = run_openscad($parametrs);
	print "Openscad server answer:<br/><textarea style='width: 96%' rows=9>$r</textarea><br/>";
	if (($p = strpos($r, "\n")) === false) $result['success'] = 0;
	else {
		$result['success'] = (substr($r, 0, $p) ? 0 : 1);
		$result['script_report'] = substr($r, $p + 1);
		//http://' . $_SERVER['REMOTE_ADDR'] . '
		$result['result_url'] =
			'http://'.$happyusers_office_ip.':83/scad_output/' . $preview_file_name;
	}
} else { // production mode
	//system($command . ' 2>&1');
	exec($command . ' 2>&1', $a, $i);
	$result['script_report'] = implode("\n", $a);
	$result['success'] = ($i ? 0 : 1);
	//http://' . $_SERVER['REMOTE_ADDR'] . '
	$result['result_url'] =
		'http://'.$happyusers_office_ip.':83/scad_output/r_php.stl';
	print "Result:<br/><textarea style='width: 96%' rows=5>$r</textarea><br/>";
}

print 'Result URL: ' . $result['result_url'] . "<br/>";
print "json result:\n<div id='json'>" . json_encode($result) . "</div><br/>";
print (microtime(true) - $time) . ' seconds of time elapsed<br/>';
print "Done.<br/>";
print "<a target='_blank' href='" . $result['result_url'] . "'>Here you can download result</a>";
// Sergiy Vovk. 2016.
?>
</pre>
</body></html>
