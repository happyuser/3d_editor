


function init() {
				//var fill_user_model_with_lattices;

	
				var materialSH =  new THREE.MeshLambertMaterial( { color:0xffffff, shading: THREE.FlatShading } );
				
				var materialNormal = new THREE.MeshNormalMaterial( { color: 0x22ff22, overdraw: 0.2, opacity: 0.6,  transparent: true  });
				var materialPhong = new THREE.MeshPhongMaterial({color: 0xFF0000});
				//var material = new THREE.MeshLambertMaterial( { color: 0x000000, opacity: 0.4,  transparent: true } );//side: THREE.DoubleSide, wireframe: true
					//alert(pos); 
				var materialTransparent = new THREE.MeshBasicMaterial( { color: 0x22ff22, overdraw: 0.2, opacity: 0.6,  transparent: true  } );//, side: THREE.DoubleSide 
				
				var materialTransparent_white = new THREE.MeshBasicMaterial( { color: 0x999999,  opacity: 0.4,  transparent: true  } );//, side: THREE.DoubleSide,overdraw: 0.2,
				var materialNormal = new THREE.MeshNormalMaterial({ color: 0x000000, side: THREE.DoubleSide, transparent:true, opacity: 0.4  });
				camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
				camera.position.z = 200;
				
				controls = new THREE.TrackballControls( camera );

				
				controls.rotateSpeed = 1.0;
				controls.zoomSpeed = 0.8;
				controls.minDistance = 80;
				controls.maxDistance = 700;
				controls.panSpeed = 0.8;

				controls.noZoom = false;
				controls.noPan = true;

				controls.staticMoving = false;
				controls.dynamicDampingFactor = 0.3;

				controls.keys = [ 65, 83, 68 ];

				controls.addEventListener( 'change', render );

				controls.object.position =  new THREE.Vector3(271.4963301460587, 266.84072956617155, -525.1061129272566);
				
				/* for f1_back version
				controls.object.position.x = 5.791697275117267;
				controls.object.position.y = -62.04883134635663;
				controls.object.position.z = 22.558857016688865;
				*/

				// world

				scene = new THREE.Scene();
				
				// Add axes
				axes = buildAxes( 1000 );
				scene.add( axes );

				//scene.fog = new THREE.FogExp2( 0xcccccc, 0.008 ); // ������ ����� �� �����
/*
				var geometry = new THREE.CylinderGeometry( 0, 10, 30, 4, 1 );
				var material =  new THREE.MeshLambertMaterial( { color:0xffffff, shading: THREE.FlatShading } );

				for ( var i = 0; i < 500; i ++ ) {

					var mesh = new THREE.Mesh( geometry, material );
					mesh.position.x = ( Math.random() - 0.5 ) * 1000;
					mesh.position.y = ( Math.random() - 0.5 ) * 1000;
					mesh.position.z = ( Math.random() - 0.5 ) * 1000;
					mesh.updateMatrix();
					mesh.matrixAutoUpdate = false;
					scene.add( mesh );

				}
  */
                var ambient = new THREE.AmbientLight( 0xffffff );
				scene.add( ambient );

				var directionalLight = new THREE.DirectionalLight( 0xffeedd ); // ������ ��������� �������
				directionalLight.position.set( 0, 0, 1 ).normalize();
				scene.add( directionalLight );
				
				
            
				// texture
				var opts = {
					color: '#000'
				} 	
				//var target = document.getElementById('progress');
				//var spinner = new Spinner(opts).spin(target);

				THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {
				     var progress = 100/total*loaded;
						if (loaded == total) {
							console.log('file loaded: '+filename+'.obj');
							document.getElementById('progress').style.display = "none";
						}

				};

				// model
				//var object1 = THREE.Mesh( new THREE.CubeGeometry( 100, 100, 100, 1, 1, 1 )); // for place holder
				console.log('loading user uploaded file: '+load_file_name);
				console.log('with file type: ' + file_type);
				var user_geometry;

				if(load_file_name != '' && file_type != '' && (file_type == 'stl' || file_type == 'STL'))
				{
					var user_loader = new THREE.STLLoader();
					user_loader.load( './files/'+load_file_name, function ( geometry ) {
		user_geometry = geometry;
		
	 
	console.log('user_geometry: ' + user_geometry);
			
		
	var latice_filename = "test"; //without ext
	//soccer ball, shell_nte3, 2.obj, optimised2, couple_paralepipeds, test, box_lattice_section, module
	$('#count_down').html('loading file ...');
	
	var o1;
	
	//				var loader = new THREE.OBJLoader( );
	//				loader.load( 'obj/'+latice_filename+'.obj', function ( object ) {
	//
	
	//				var loader = new THREE.OBJMTLLoader(); //������ ���������� three_66.js 
	//					loader.load( 'obj/'+latice_filename+'.obj', 'obj/'+latice_filename+'.mtl', function ( object ) {
	
	//************************************************************************************
	if(lattice_file_name != "")
	{;}
	else
	{
		lattice_file_name = "module.stl";
	}
	console.log('lattice_file_name:'+lattice_file_name);
		
	return;
	var loader = new THREE.STLLoader();
	loader.load( './stl/'+lattice_file_name, function ( geometry ) {
		
	$('#count_down').html('creating scene ...');
	
			var mesh = new Array();
	//var material = new Array();
	var m = 1;
	var pos;
	
	var mesh = new THREE.Mesh(geometry);
	var b_l = new THREE.Box3().setFromObject( mesh ); //box_lattice
	//console.log( b_l.min, b_l.max, b_l.size() );
	//return;
	
	var big_geometry = new THREE.Geometry();
	user_mesh = new THREE.Mesh(user_geometry, materialTransparent_white);
	//scene.add(user_mesh);
	
	var box = new THREE.Box3().setFromObject( user_mesh );
	console.log( box.min, box.max, box.size() );
	var u_size = box.size();
	
	// number of lattices in user object bounding box - axis x
	var sc = 1; // 6 in a row is maximum
	if(x_lattices_count != "")
		sc = x_lattices_count;
		
	console.log('x_lattices_count: '+x_lattices_count);
	//return;
	var l_scale = 1/sc; // one lattice size / user object size
	
	//get needed size of lattice on x vertice
	var c_x = u_size.x/sc;
	
	//get lattice geometry  scale ratio (geometry got from file - 58px to ?)
	var l_file_scale = c_x / b_l.size().x;
	
	//sizes of lattice  
	var c_y = b_l.size().y*l_file_scale ;
	var c_z = b_l.size().y*l_file_scale ;
	
	//get latices count on vertices
	var x_length = Math.ceil(u_size.x/c_x);
	var y_width = Math.ceil(u_size.y/c_y);
	var z_height = Math.ceil(u_size.z/c_z);
	
	var faces_count = geometry.faces.length*z_height*y_width*x_length;
	//console.log('user_geometry.faces.length'+user_geometry.faces.length);
		
	//start count down for intersaction process
	console.log('faces count in big_geometry is: '+faces_count);
	var time_amount_0 = 56; //91s for 17972 obj file faces , 56s for 5364 stl file 
	var faces_count_0 = 5364+1200;
	var time_left = Math.ceil(faces_count / faces_count_0 * time_amount_0);
	start_count_down(time_left, true);
	
	var objectBSP;
	var b_l_s = new THREE.Box3();; //box_lattice_scaled 
	
	for(z = 0; z<z_height; z++)
	{
		for(y = 0; y<y_width; y++)
		{
			for(pos = 0; pos<x_length; pos++)
			{
				mesh[pos] = new THREE.Mesh( geometry );
				mesh[pos].scale.set(l_file_scale, l_file_scale, l_file_scale); //scale the lattice geometry
				if(pos == 0) 
					b_l_s = new THREE.Box3().setFromObject( mesh[pos] );
				
				mesh[pos].position.x = box.min.x - b_l_s.min.x + pos*(b_l_s.size().x);
				mesh[pos].position.y = box.min.y - b_l_s.min.y + y*(b_l_s.size().y);
				mesh[pos].position.z = box.min.z - b_l_s.min.z + z*(b_l_s.size().z);
				
				
				THREE.GeometryUtils.merge(big_geometry, mesh[pos]);
				
				//break;
			}//pos
					
		}//y
	}//z
	
	user_geometry.dynamic = true;
	user_mesh = new THREE.Mesh(user_geometry);
	//user_mesh.scale.set(1/l_scale,1/l_scale,1/l_scale);
	user_mesh.position.set(0, 0, 0);
	
	//***************************************************
	//to fill gaps that will left after intersaction
	THREE.GeometryUtils.merge(big_geometry, user_mesh);
	//        
	//put merged latice material to scene
	big_geometry.dynamic = true;
	var big_mesh = new THREE.Mesh(big_geometry, materialNormal);
	//*******\
	//scene.add(big_mesh);
	//user_mesh.position.set(-2*b_l.size().x, 0, 0);
	//scene.add(user_mesh);
	
	//return;
	
	// merged lattices intersect Sphere
	objectBSP = new ThreeBSP( big_mesh);
	userBSP = new ThreeBSP( user_mesh);
	
	//var cubeGeometry = new THREE.CubeGeometry( 100, 100, 100, 1, 1, 1 );
	//		var cubeMesh = new THREE.Mesh( cubeGeometry );
	//		var cubeBSP = new ThreeBSP( cubeMesh );
	//		
	// **************** 
	//	var sphere_radius = 155;
	//		var sphereGeometry = new THREE.SphereGeometry( sphere_radius, 25, 25 );
	//		var sphereMesh = new THREE.Mesh( sphereGeometry, materialTransparent);
	//		var sphereBSP = new ThreeBSP( sphereMesh );
	//		//console.log('sphereGeometry.faces.length: '+sphereGeometry.faces.length);
	
	//return;
	
	//***** long operation !!!!!!!!!
	console.log('!!!! creating sphereBSP.intersect( objectBSP )');
	var timerStart = Date.now();
	var newBSP = objectBSP.intersect( userBSP); // objectBSP  cubeBSP, userBSP, sphereBSP
	var time_used = Math.ceil((Date.now()-timerStart)/1000);
	console.log("Time used for intersection: ", time_used);
	
	
	var timerStart = Date.now();
	console.log('creating newMesh = newBSP.toMesh( materialTransparent )');
	var newMesh = newBSP.toMesh( materialNormal); //materialNormal, materialTransparent_white, materialTransparent 
	console.log("Time until intersect: ", Math.ceil((Date.now()-timerStart)/1000));
	console.log('-u_size.x/2:'+-u_size.x/2);
	//newMesh.position.set(0, 0, 0);
	//newMesh.position.set(-u_size.x/2, -u_size.y/2, -u_size.z/2); //(-u_size.x/2, -u_size.y/2, -u_size.z/2), 
	//(-70, 60, -120) 
	//get user model scale to best view;
	var u_model_s_v = 500/u_size.x;
	newMesh.scale.set(u_model_s_v,u_model_s_v,u_model_s_v);
	
	//**************************
	scene.add( newMesh );
	$('#count_down').html('Done, in '+ time_used +' seconds');
	setTimeout(function() {$('#count_down').hide("slow");}, 5000);
	//console.log('return line 216');return;
	
	newMesh.geometry.verticesNeedUpdate = true;
	
	
	 setTimeout(function ( ) {
				user_geometry = geometry;
				
	 
	 	scene.add( newMesh );
	 	newMesh.geometry.verticesNeedUpdate = true;
	}, 2000);
	//scene.add( cubeMesh  );
	//		scene.add( sphereMesh  );
	//		scene.add( object );
	//	    
	//		scene.add(big_mesh);
	//		scene.add(user_mesh);
	//		//scene.add( newMesh );
	            			
			
	} );//end of User OBJloader
	
	
});//user_loader.load( './files/'+load_file_name
					
				}//if(load_file_name != ''
                

				// lights
				var light = new THREE.HemisphereLight(0xfffff0, 0x101020, 1.25);
			    light.position.set(0.75, 1, 0.25);
			    scene.add(light);
				

				light = new THREE.DirectionalLight( 0xccffff );
				light.position.set( 1, 1, 1 );
				scene.add( light );


				light = new THREE.DirectionalLight( 0x888888 );
				light.position.set( -1, -1, -1 );
				scene.add( light );

				light = new THREE.AmbientLight( 0x222222 );
//				light.position.set( -1, -1, -1 );
				scene.add( light );

				// renderer

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				//renderer.setClearColor( scene.fog.color, 1 ); // Fog
				renderer.setSize( window.innerWidth, window.innerHeight );

				container = document.getElementById( 'container' );
				container.appendChild( renderer.domElement );

				/*stats = new Stats();
				stats.domElement.style.position = 'absolute';
				stats.domElement.style.top = '0px';
				stats.domElement.style.zIndex = 100;
				container.appendChild( stats.domElement );
                */
				//

				window.addEventListener( 'resize', onWindowResize, false );

			}

