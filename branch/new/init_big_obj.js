var View = function() {
	
	this.materials =  new Array();
	this.scene 	   =  new THREE.Scene();
	this.camera    =  new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
	this.controls  =  new THREE.TrackballControls( this.camera );
	this.renderer  =  new THREE.WebGLRenderer({ antialias: true });

	this.resize_canvas = function() {

		var self = this;

		$("#container canvas").width('100%');
		$("#container canvas").height($("#container").width()*window.innerHeight/window.innerWidth );

		$("#container canvas").mouseout(function (){

			self.controls.enabled = false;

		});

		$("#container canvas").mouseover(function (){

			self.controls.enabled = true;

		});

	};

};

View.prototype = {

	scale_to500_and_move_to0: function(mesh, scale_to_size) {

		var u_box = new THREE.Box3().setFromObject( mesh );
	
		var u_size = u_box.size();
	
			mesh.scale.set(scale_to_size/u_size.x, scale_to_size/u_size.x, scale_to_size/u_size.x);
			mesh =  this.move_to_0(mesh);

		return mesh;	

	},

	move_to_0: function(mesh) {

		u_box = new THREE.Box3().setFromObject( mesh );
	
		return  this.move_to_0_box(mesh, u_box);
	
	},

	move_to_0_box: function(mesh, box) {

		p = new THREE.Vector3(-(u_box.min.x + u_box.max.x)/2, -(u_box.min.y + u_box.max.y)/2, -(u_box.min.z + u_box.max.z)/2);

		this.set('options', mesh, {

			position: p

		});

		return mesh;

	},

	show_bounds_box: function(u_size, scene, material) {
		
		u_bounds = new THREE.Mesh( new THREE.BoxGeometry( u_size.x, u_size.y, u_size.z ), material );
		mesh.position.copy(box.min.add(box.max));

		this.set('options', u_bounds, {

			position: {

				x: (box.min.x + box.max.x)/2,
				y: (box.min.y + box.max.y)/2,
				z: (box.min.z + box.max.z)/2

			}

		});

		this.scene.add(u_bounds);

		return this.scene;
	},

/*

	Generate axes x, y, z

	For example:

		this.buildAxes( length of axis ) );

*/	
	buildAxes: function( length ) {

		var axes = new THREE.Object3D();

			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( length, 0, 0 ), 0xFF0000, false ) ); // +X
			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( -length, 0, 0 ), 0xFF0000, true) ); // -X
			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, length, 0 ), 0x00FF00, false ) ); // +Y
			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, -length, 0 ), 0x00FF00, true ) ); // -Y
			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, length ), 0x0000FF, false ) ); // +Z
			axes.add( this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, -length ), 0x0000FF, true ) ); // -Z

		return axes;

	},

/*

	this.buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( length, 0, 0 ), 0xFF0000, false ) );

*/	

	buildAxis: function( src, dst, colorHex, dashed ) {

		var geometry = new THREE.Geometry(),
			material; 

		if(dashed) {

			material = new THREE.LineDashedMaterial({ linewidth: 3, color: colorHex, dashSize: 3, gapSize: 3 });

		} else {

			material = new THREE.LineBasicMaterial({ linewidth: 3, color: colorHex });

		}

		geometry.vertices.push( src.clone() );
		geometry.vertices.push( dst.clone() );
		geometry.computeLineDistances(); 

		var axis = new THREE.Line( geometry, material, THREE.LinePieces );

		return axis;

	},

	change_lattice_model: function() {

		switch(lattice_id) {

		  	case 'el_5':  

		    	lattice_file_name = 'unitCell.stl';

		    break
		  	case 'el_6': 

		    	lattice_file_name = "simple2a.stl";

		    break		
			case 'el_7':  

		    	lattice_file_name = "simple1a.stl";

		    break
			case 'el_8': 

		    	lattice_file_name = "simple3a.stl";

		   	break
			case 'el_9': 
				default:
		    		lattice_file_name = "module.stl";
		   	break

		}
	},

/*

	Build and load complite element

	For example:

		this.load('/path/to/source/', material );

*/

	load: function(pathtofiles, material) {

		var user_geometry;

		if(load_file_name != '' && file_type != '' && (file_type == 'stl' || file_type == 'STL')) {

			count_down_show();

			var user_loader = new THREE.STLLoader();

			user_loader.load( pathtofiles + load_file_name, function ( geometry ) {

				user_geometry = geometry;
						
				user_mesh = new THREE.Mesh(user_geometry, material);
			
		
				$('#count_down').html('loading file ...');

					if(lattice_id != "" && (typeof lattice_id !== 'undefined')) {

						this.change_lattice_model();

					} else {
						
						
						this.add_to('scene', 

							{

								figure: this.scale_to500_and_move_to0(user_mesh, 100)

							}

						);
							
						this.resize_canvas();

						count_down_hide();

						return;

					}
	
			}.bind(this));
		}

		count_down_show();

		$('#count_down').html('creating scene ...');

	},

	render: function() {

		var scene = view.scene; 
		var camera = view.camera;

		view.renderer.render( scene, camera );

	},

	animate: function() {

		requestAnimationFrame(view.animate);
		view.render();
		view.controls.update();

	},

	set: function(text, target, options) {

/*

Must be a THREE.js object
For example:

	set('options', element, {
	
		rotateSpeed : 1.0,
		zoomSpeed : 0.8,
		minDistance : 80,
		maxDistance : 700,
		panSpeed : 0.8,
		noZoom : false,
		noPan : true,
		staticMoving : false,
		dynamicDampingFactor : 0.3,
		keys : [ 65, 83, 68 ],
		enabled : false	

	})

*/

		if ( text == 'options' || text == 'undefined' ) {

			for (var item in options) {
				target[item] = options[item];
			}

		}

	},

/*

For example:

	get('materials', 'name', 'cube') ===> 

		If all is ok, function must return object with name - 'cube'

*/
	get: function(target, by, name) {

		switch( target ) {

			case 'materials':

				var material = this.materials;

				for( var i = 0; i < material.length; i++ ) {

					if (material[i].name == name) {

						return material[i];
						break;

					} else {

						console.log('Material with name: ' + name + ' is"nt found :(');
						break;

					}

				}

			break
			case 'scene':

				var scene = this.scene.children;

				for (var i in scene) {

					if (scene[i].name == name) {

						return scene[i];
						break;

					} 

				}

			break
		}

	},

	add_to: function(target, objects) {

		switch( target ) {

/*

For 'scene', use like this ==>

	{

		name: some obj or new THREE constructor,
		name: some obj or new THREE constructor,
		name: some obj or new THREE constructor,
		name: some obj or new THREE constructor,

		...

	}	

*/

			case 'scene':
				var scene = this.scene;
				
				for (var item in objects) {

					objects[item].name = item;
					scene.add(objects[item]);
					
				}
				
			break

/*

For 'materials', use like this ==>

	{
		name: new THREE material constructor ({

			ambient	 	: prop,
			color 		: prop,
			specular 	: prop,
			shininess	: prop,
			shading	 	: prop,
			opacity	 	: prop,
			metal       : prop,
			transparent : prop

		}), ...
	}

*/

			case 'materials':
				var materials = this.materials;
				
				for (var item in objects) {

					objects[item].name = item;
					materials.push(objects[item]);
					
				}
				
			break
		}

	}	

}

var view;
var camera;
var controls;
var scene;
var renderer;


function init() {
							
	count_down_show();

	view = new View();

	scene = view.scene;
	camera = view.camera;
	controls = view.controls;
	renderer = view.renderer;


/*

Add new material for rendered object

*/

	view.add_to('materials',
		{
			cube: new THREE.MeshPhongMaterial({

				ambient	 	: 0x030303,
				color 		: 0xcccccc,
				specular 	: 0xFFEE7C,
				shininess	: 10,
				shading	 	: THREE.FlatShading,
				opacity	 	: 0.6,
				metal       : true,
				transparent : true

			})

		}
	);

/*

Add objects to scene view

*/

	view.add_to('scene',
		{
			 axes: view.buildAxes( 1000 ),
			 light: new THREE.HemisphereLight(0xfffff0, 0x101020, 1),
			 ambient: new THREE.AmbientLight( 0xffbbbb ),
			 spotLight: new THREE.SpotLight( 0xffffff )
		}
	);

/*

Set options of spotLight object

*/

	view.set('options', view.get('scene', 'name', 'spotLight'), {

		position: {

			x : -250,
			y : 250,
			z : 250

		},

		castShadow 		 : true,
		shadowMapWidth   : 1024,
		shadowMapHeight  : 1024,
		shadowCameraNear : 500,
		shadowCameraFar  : 4000,
		shadowCameraFov  : 1

	});

/*

Set options of light object

*/

	view.set('options', view.get('scene', 'name', 'light'), {

		position: {

			x: 0.75,
			y: 1,
			z: 0.25

		}

	});


/*

Set camera position

*/
	view.set('options', camera.position, {

		z: 300

	}); 
/*

Set camera.controls options

*/
	view.set('options', controls, {

		rotateSpeed 		 : 	1.0,
		zoomSpeed 			 : 	0.8,
		minDistance 		 : 	80,
		maxDistance 		 : 	700,
		panSpeed 			 : 	0.8,
		noZoom 				 : 	false,
		noPan 				 : 	true,
		staticMoving		 : 	false,
		dynamicDampingFactor : 	0.3,
		keys 				 : 	[ 65, 83, 68 ],
		enabled 			 : 	false

	});

	controls.addEventListener( 'change', view.render );

/*

Loading resources

view.load(files, material)

*/	
	
	view.load('./files/', view.get('materials', 'name', 'cube'));
					
          
	count_down_hide();

	
	renderer.setClearColor( 0xcccccc, 1 ); 
	renderer.setSize( window.innerWidth, window.innerHeight );

	container = $('#container');
	container.append( renderer.domElement );

	view.resize_canvas();

	window.addEventListener( 'resize', function() {

		camera.aspect = $("#container canvas").width() / $("#container canvas").height();
		camera.updateProjectionMatrix();

		renderer.setSize( $("#container canvas").width(), $("#container canvas").height() );
	
	}, false );
								
}
