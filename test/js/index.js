		if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

		var container, stats;

		var camera, controls, scene, renderer;

		var cross;

		var raycaster = new THREE.Raycaster();
			var mouse = new THREE.Vector2(),
			offset = new THREE.Vector3(),
			INTERSECTED, SELECTED;

		var mesh;
		var objects = [], plane;

		init();
		animate();

		function init() {

			camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
			camera.position.set(-250, 250, 500);

			controls = new THREE.OrbitControls( camera );
			controls.target.set = new THREE.Vector3(0, 0, 0);
			controls.minDistance = 10;
			controls.maxDistance = 250;
			controls.maxPolarAngle = Math.PI;

			// world

			scene = new THREE.Scene();
			scene.fog = new THREE.FogExp2(0xcce0ff, 0.0003);

			var ambient = new THREE.AmbientLight( 0xffffff );
			scene.add( ambient );

			// texture

			THREE.DefaultLoadingManager.onProgress = function ( item, loaded, total ) {
					if (loaded == total) {
						document.getElementById('progress').style.display = "none";
					}

			};

			// model

			var loader = new THREE.STLLoader();

			// ASCII file

			loader.load( './stl/test.stl', function ( geometry ) {
				var material = new THREE.MeshPhongMaterial();
				var mesh = new THREE.Mesh( geometry, material );
				mesh.castShadow = true;
				mesh.receiveShadow = true;
				mesh.rotateZ(1.53);
				mesh.position.x = 29.88;
				mesh.position.y = -26.69;
				mesh = mesh;
				scene.add( mesh );
				objects.push( mesh );
			} );

			// Binary files

			var material = new THREE.MeshPhongMaterial( { ambient: 0x050505, color: 0x0033ff, specular: 0x555555, shininess: 30 } );

			loader.load( './stl/cube.stl', function ( geometry ) {

				var mesh = new THREE.Mesh( geometry, material );


				mesh.castShadow = true;
				mesh.receiveShadow = true;

				// mesh.position.copy( camera.position );
				// mesh.rotation.copy( camera.rotation );
				// mesh.updateMatrix();
				// mesh.translateZ( - 10 );

				scene.add( mesh );
				objects.push( mesh );
				console.log (mesh.id);

			} );

			plane = new THREE.Mesh(
				new THREE.PlaneBufferGeometry( 800, 800, 8, 8 ),
				new THREE.MeshBasicMaterial( { visible: false } )
			);
			scene.add( plane );

			// lights

			directionalLight = new THREE.DirectionalLight( 0x222222 );
			directionalLight.position.set( 1, 1, 1 );
			scene.add( directionalLight );

			ambientLight = new THREE.AmbientLight( 0x222222 );
			ambientLight.position.set( -1, -1, -1 );
			scene.add( ambientLight );

			// renderer

			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.setClearColor( scene.fog.color, 1 ); // Fog
			renderer.setSize( window.innerWidth, window.innerHeight );
			renderer.shadowMap.enabled = true;
			renderer.shadowMapSoft = true;

			container = document.getElementById( 'container' );
			container.appendChild( renderer.domElement );

			stats = new Stats();
			stats.domElement.style.position = 'absolute';
			stats.domElement.style.top = '0px';
			stats.domElement.style.zIndex = 100;
			container.appendChild( stats.domElement );


			// when the mouse moves, call the given function
			document.addEventListener( 'mousemove', onDocumentMouseMove, false );
			document.addEventListener( 'mousedown', onDocumentMouseDown, false );
			document.addEventListener( 'mouseup', onDocumentMouseUp, false );

			window.addEventListener( 'resize', onWindowResize, false );

		}

		function onDocumentMouseMove( event ) {

			event.preventDefault();

			mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
			mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

			//

			raycaster.setFromCamera( mouse, camera );

			if ( SELECTED ) {

				var intersects = raycaster.intersectObject( plane );

				if ( intersects.length > 0 ) {

					SELECTED.position.copy( intersects[ 0 ].point.sub( offset ) );

				}

				return;

			}

			var intersects = raycaster.intersectObjects( objects );

			if ( intersects.length > 0 ) {

				if ( INTERSECTED != intersects[ 0 ].object ) {

					if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );

					INTERSECTED = intersects[ 0 ].object;
					INTERSECTED.currentHex = INTERSECTED.material.color.getHex();

					plane.position.copy( INTERSECTED.position );
					plane.lookAt( camera.position );

				}

				container.style.cursor = 'pointer';

			} else {

				if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );

				INTERSECTED = null;

				container.style.cursor = 'auto';

			}

		}

		function onDocumentMouseDown( event ) {

			event.preventDefault();

			raycaster.setFromCamera( mouse, camera );

			var intersects = raycaster.intersectObjects( objects );

			if ( intersects.length > 0 ) {

				console.log(intersects[ 0 ].object.id);

				controls.enabled = false;

				SELECTED = intersects[ 0 ].object;

				var intersects = raycaster.intersectObject( plane );

				if ( intersects.length > 0 ) {

					offset.copy( intersects[ 0 ].point ).sub( plane.position );

				}

				container.style.cursor = 'move';

			}

		}

		function onDocumentMouseUp( event ) {

			event.preventDefault();

			controls.enabled = true;

			if ( INTERSECTED ) {

				plane.position.copy( INTERSECTED.position );

				SELECTED = null;

			}

			container.style.cursor = 'auto';

		}

		function animate_camera() {

				var tween = new TWEEN.Tween(camera.position.z).to( 10, 3000).easing(TWEEN.Easing.Quadratic.InOut).onUpdate(function () {
						camera.lookAt(scene.position);
				}).onComplete(function () {
						camera.lookAt(scene.position);
				}).start();
		}

		function onWindowResize() {

			camera.aspect = window.innerWidth / window.innerHeight;
			camera.updateProjectionMatrix();

			renderer.setSize( window.innerWidth, window.innerHeight );

			controls.handleResize();

			render();

		}

		function animate() {

			requestAnimationFrame( animate );
			render();
			controls.update();

		}

		function render() {
			renderer.render( scene, camera );
			stats.update();

		}

		//spinner
		var opts = {
			color: '#000'
		}

		var target = document.getElementById('progress');
		var spinner = new Spinner(opts).spin(target);