names = ["torus.stl","cone.stl","cube.stl","monkey.stl"];
modules = ["module.stl"];

user_file=names[0];   // must be initalised
user_filename=user_file; // param1 passed via -D on cmd-line
echo(user_file);

lattice=modules[0];   // must be initalised
l_filename = lattice;//
// param1 passed via -D on cmd-line
echo(l_filename);

module u_object()
{
        
    //
    echo(names[2] );

    //next this will be the generated inner wall object
    translate([object_size_x/2,object_size_x/2,object_size_x/2])
    resize([100,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
    import(user_filename, convexity=3, center=true);
}

module l(){
    color([0.5,0,0.5,0.2])
    import("module.stl", convexity=3, center=true);
    
}


intersection(){

color([0.5,0.5,0,0.2]) u_object();
translate([-100,0,-100])cube(size = 200, center = false);
    
//l();
}


