// CSG.scad - Basic example of CSG usage
/*color([0.5,0.5,0,0.8])
import("cube.stl", convexity=3, material=transparent);
*/
color([0.5,0.5,0,0.8]) cube(size=[10,10,10], center=true);
color([0,0.5,0.5,0.3]) cube(size=[20,20,20], center=true);

translate([-24,0,0]) {
    union() {
        cube(15, center=true);
        sphere(10);
    }
}

intersection() {
    cube(15, center=true);
    sphere(10);
}

translate([24,0,0]) {
    difference() {
        cube(15, center=true);
        sphere(10);
    }
}

echo(version=version());
// Written by Marius Kintel <marius@kintel.net>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
