names = ["torus.stl","cone.stl","cube.stl","monkey.stl","monkey_0.stl"];
modules = ["module.stl","simple1a.stl","simple2a.stl","simple3a.stl","unitCell.stl"];

user_file=names[1];   // must be initalised
user_filename=user_file; // param1 passed via -D on cmd-line
echo(user_file);

lattice=modules[2];   // must be initalised
l_filename = lattice;//
// param1 passed via -D on cmd-line
echo(l_filename);

include <TOUL.scad>;
//include <strings.scad>;
l_scale="50";   // must be initalised
//l_file_scale = 30;
l_file_scale = atoi(l_scale)/100;//
// param1 passed via -D on cmd-line
echo(str("l_file_scale: ",l_file_scale));

//$parametrsNames='l_s_x,l_s_y,l_s_z,u_s_x,u_s_y,u_s_z,l_scale'; 

module u_object()
{
        
    //
    object_size_x = 100;
    //next this will be the generated inner wall object
    translate([object_size_x/2,object_size_x/2,object_size_x/2])
    resize([100,0,0], auto=true) //scales to 100 x size.
        //color([0.5,0,0,0.2]) 
    import(user_filename, convexity=3, center=true);
}

module l(){
    color([0.5,0,0.5,0.2])
    import(l_filename, convexity=3, center=true);
    
}

//resize([100,100,100], auto=false)color([0.5,0.5,0,0.2]) l();
//color([0.5,0.5,0,0.2]) u_object();

object_size_x = 100;
l_size = object_size_x * l_file_scale; 
//echo(l_size);

x_size = 100;
y_size = 100;
z_size = 100;
//xmax - xmin;// = object_size_x
//echo(l_size );

x_count = x_size/l_size ;
y_count = y_size/l_size ;
//echo(y_count);
z_count = z_size/l_size ;

b_l_s_min_x = -l_size/2;
b_l_s_min_y = -l_size/2;
b_l_s_min_z = -l_size/2;

box_min_x = 0;
box_min_y = 6;
box_min_z = 0;

/*
union() {
    wall_thickness = 1;
    translate([-1 ,-1 ,-1])
    color([0,0,0.5,0.2])    u_object();
    union() {
        color([0,0.5,0,0.2])    u_object();
            //add starting object like user object*/
        intersection(){  
            color([0.5,0.5,0,0.4])    
            resize([98,0,0], auto=true)
              u_object();
            color([0.5,0,0.5,0.8])
            for(x = [0:1:x_count-1], y= [0:y_count-1], z= [0:y_count-1]) {
                pos_x = box_min_x - b_l_s_min_x + x*(l_size);
                pos_y = box_min_y - b_l_s_min_y + y*(l_size);
                pos_z = box_min_z - b_l_s_min_z + z*(l_size);
                
            translate([pos_x ,pos_y ,pos_z])
            resize(newsize=[l_size,l_size,l_size])
                l();
                    
                    //break;
            }//pos
        }
        
    //} //first covering
//}//LAST covering object

//monkey.stl
//box_lattice_section.stl
echo(version=version());
// Written by Marius Kintel <marius@kintel.net>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
